import { Flex, Text, Box, Grid, Progress } from "theme-ui";
import React, { FC } from "react";
import Heading from "../components/Heading";
import ResumeItem from "../components/ResumeItem";
import { useRouter } from "next/router";
import en from "../locales/en";
import vi from "../locales/vi";

const Resume: FC = () => {
  const router = useRouter();
  const t = router.locale === "en" ? en : vi;

  return (
    <Box>
      <Heading label={t.resume} content={t.content4} />
      <Flex
        sx={{
          justifyContent: "center",
        }}
      >
        <Grid
          my={[10, 20, 30]}
          columns={[1, 1, 2]}
          sx={{
            maxWidth: ["90%", "70%", "55%"],
          }}
        >
          <Box
            my={[10, 15, 20]}
            mr={15}
            sx={{
              justifyContent: "center",
            }}
          >
            <Flex
              mb={25}
              sx={{
                flexDirection: "column",
              }}
            >
              <Text
                mb="7px"
                sx={{
                  fontSize: [16, 18, 22],
                  fontWeight: 600,
                  opacity: 0.8,
                }}
              >
                {t.education}
              </Text>
              <Progress
                max={1}
                value={1 / 3}
                sx={{
                  width: "20%",
                  height: "2px",
                }}
              />
            </Flex>
            <ResumeItem
              date="2015 - 2021"
              place={t.place}
              label={t.label}
              content={t.content5}
            />
          </Box>
          <Box
            my={[10, 15, 20]}
            mr={15}
            sx={{
              justifyContent: "center",
            }}
          >
            <Flex
              mb={25}
              sx={{
                flexDirection: "column",
              }}
            >
              <Text
                mb="7px"
                sx={{
                  fontSize: [16, 18, 22],
                  fontWeight: 600,
                  opacity: 0.8,
                }}
              >
                {t.work}
              </Text>
              <Progress
                max={1}
                value={1 / 3}
                sx={{
                  width: "30%",
                  height: "2px",
                }}
              />
            </Flex>
            <ResumeItem
              date="7/2018 - 2019"
              place={t.place1}
              label={t.label1}
              content={t.content6}
            />

            <ResumeItem
              date="4/2021 - 5/2021"
              place={t.place2}
              label={t.label2}
              content={t.content7}
            />
          </Box>
          <Box
            my={[10, 15, 20]}
            mr={15}
            sx={{
              justifyContent: "center",
            }}
          >
            <Flex
              mb={25}
              sx={{
                flexDirection: "column",
              }}
            >
              <Text
                mb="7px"
                sx={{
                  fontSize: [16, 18, 22],
                  fontWeight: 600,
                  opacity: 0.8,
                }}
              >
                {t.skills}
              </Text>
              <Progress
                max={1}
                value={1 / 3}
                sx={{
                  width: "20%",
                  height: "2px",
                }}
              />
            </Flex>
            <Flex
              ml={25}
              sx={{
                flexDirection: "column",
                fontSize: [0, 1, 2],
                opacity: 0.8,
                lineHeight: "1.75em",
                borderLeft: "2px solid rgb(30,167,253, 0.2)",
              }}
            >
              <Text my="5px" ml={10}>
                {t.skill1}
              </Text>
              <Text my="5px" ml={10}>
                {t.skill2}
              </Text>
              <Text my="5px" ml={10}>
                Framework: ReactJS
              </Text>
              <Text my="5px" ml={10}>
                Platform: Nodejs, Typescript-nextjs
              </Text>
              <Text my="5px" ml={10}>
                {t.skill3}
              </Text>
              <Text my="5px" ml={10}>
                {t.skill4}
              </Text>
              <Text my="5px" ml={10}>
                {t.skill5}
              </Text>
            </Flex>
          </Box>
        </Grid>
      </Flex>
    </Box>
  );
};

export default Resume;
