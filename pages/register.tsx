import { Flex, Button, Label, Input, Text, Link, Box } from "theme-ui";
import React, { useEffect, useState, useRef } from "react";
import { useRouter } from "next/router";
import { useForm } from "react-hook-form";
import axios from "axios";
import Layout from "../containers/layout";
import Heading from "../components/Heading";
import en from "../locales/en";
import vi from "../locales/vi";

const register = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    watch,
  } = useForm();

  const [username, setUsername] = useState<String>("");
  const [password1, setPassword1] = useState<String>("");
  const [isClick, SetIsClick] = useState(true);
  const router = useRouter();
  const t = router.locale === "en" ? en : vi;
  const password = useRef<HTMLDivElement | null>(null);
  password.current = watch("password", "");
  const [mess, setMess] = useState(false);

  useEffect(() => {
    const fetchAccount = async () =>
      username &&
      axios({
        method: "POST",
        url: "https://cv-web-server.herokuapp.com/register",
        data: {
          username: username,
          password: password1,
        },
      })
        .then((res) => {
          console.log(res.data);
          if (res.status === 201) {
            router.push("/login");
          } else {
            setMess(true);
          }
        })
        .catch((err) => {
          console.log(err);
        });
    setUsername("");
    setPassword1("");
    fetchAccount();
  }, [isClick]);

  const onSubmit = async (data: { username: String; password: String }) => {
    setUsername(data.username);
    setPassword1(data.password);
    SetIsClick(!isClick);
  };

  return (
    <Box>
      <Heading label={t.signup} content={t.contentsignup} />
      <Flex
        mt={30}
        sx={{
          width: "100%",
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "column",
        }}
      >
        <Flex
          as="form"
          onSubmit={(e) => e.preventDefault()}
          sx={{
            flexDirection: "column",
            width: [150, 200, 250],
          }}
        >
          <Label
            htmlFor="username"
            mb="3px"
            sx={{
              fontSize: [0, 1, 17],
              fontWeight: 600,
            }}
          >
            {t.username}
          </Label>
          <Input
            sx={{
              transition: "all 0.3s ease-in-out 0s",
              fontSize: [10, 0, 15],
              border: "1px solid grey",
            }}
            placeholder={t.inputUsername}
            {...register("username", {
              required: true,
            })}
          />
          {errors.username && (
            <Text color="red" mt="3px">
              {t.requiredUsername}
            </Text>
          )}
          <Label
            htmlFor="password"
            mt={10}
            mb="3px"
            sx={{
              fontSize: [0, 1, 17],
              fontWeight: 600,
            }}
          >
            {t.password}
          </Label>
          <Input
            type="password"
            placeholder={t.inputPassword}
            sx={{
              transition: "all 0.3s ease-in-out 0s",
              fontSize: [10, 0, 15],
              border: "1px solid grey",
            }}
            {...register("password", {
              required: t.requiredPassword,
              minLength: {
                value: 8,
                message: t.limit,
              },
            })}
          />
          {errors.password && (
            <Text color="red" mt="3px">
              {errors.password.message}
            </Text>
          )}
          <Label
            htmlFor="password"
            mt={10}
            mb="3px"
            sx={{
              fontSize: [0, 1, 17],
              fontWeight: 600,
            }}
          >
            {t.repassword}
          </Label>
          <Input
            type="password"
            placeholder={t.inputrePassword}
            sx={{
              transition: "all 0.3s ease-in-out 0s",
              fontSize: [10, 0, 15],
              border: "1px solid grey",
            }}
            {...register("password_repeat", {
              validate: (value) => value === password.current || t.confirm1,
            })}
          />
          {errors.password_repeat && (
            <Text color="red" mt="3px">
              {errors.password_repeat.message}
            </Text>
          )}
          <Button
            type="submit"
            onClick={handleSubmit(onSubmit)}
            my={25}
            sx={{
              opacity: 0.7,
              fontSize: [0, 1, 17],
              transition: "all 0.3s ease-in-out 0s",
              fontWeight: 600,
              cursor: "pointer",
              ":active": {
                transform: "scale(0.95)",
              },
              ":hover": {
                opacity: 1,
              },
            }}
          >
            {t.signup}
          </Button>
          {mess && (
            <Text
              color="white"
              py={10}
              pl={10}
              mb={20}
              bg="red"
              sx={{
                fontSize: [10, 0, 15],
                borderRadius: 4,
                opacity: 0.7,
                width: "100%",
              }}
            >
              {t.wrongRegister}
            </Text>
          )}
        </Flex>
      </Flex>
    </Box>
  );
};

export default register;
