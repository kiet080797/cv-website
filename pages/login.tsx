import { Flex, Button, Label, Input, Text, Link, Box } from "theme-ui";
import React, { useEffect, useState, useCallback } from "react";
import { useRouter } from "next/router";
import { useForm } from "react-hook-form";
import axios from "axios";
import Layout from "../containers/layout";
import Heading from "../components/Heading";
import en from "../locales/en";
import vi from "../locales/vi";

const login = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const [username, setUsername] = useState<String>("");
  const [password, setPassword] = useState<String>("");
  const [isClick, SetIsClick] = useState(true);
  const router = useRouter();
  const t = router.locale === "en" ? en : vi;
  const [mess, setMess] = useState(false);

  useEffect(() => {
    const fetchAccount = async () =>
      username &&
      axios({
        method: "POST",
        url: "https://cv-web-server.herokuapp.com/login",
        data: {
          username: username,
          password: password,
        },
      })
        .then((res) => {
          if (res.status === 200) {
            localStorage.setItem("token", res.data.token);
            localStorage.setItem("username", res.data.username);
            router.reload();
          } else {
            console.log(res.data);
            setMess(true);
          }
        })
        .catch((err) => console.log(err));

    setUsername("");
    setPassword("");
    fetchAccount();
  }, [isClick]);

  const onSubmit = (data: { username: String; password: String }) => {
    setUsername(data.username);
    setPassword(data.password);
    SetIsClick(!isClick);
  };
  return (
    <Box>
      <Heading label={t.login} content={t.contentLogin} />
      <Flex
        mt={30}
        sx={{
          width: "100%",
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "column",
        }}
      >
        <Flex
          as="form"
          onSubmit={(e) => e.preventDefault()}
          sx={{
            flexDirection: "column",
            width: [150, 200, 250],
          }}
        >
          <Label
            htmlFor="username"
            mb="3px"
            sx={{
              fontSize: [0, 1, 17],
              fontWeight: 600,
            }}
          >
            {t.username}
          </Label>
          <Input
            sx={{
              transition: "all 0.3s ease-in-out 0s",
              fontSize: [10, 0, 15],
              border: "1px solid grey",
            }}
            defaultValue="kiet12345678"
            placeholder={t.inputUsername}
            {...register("username", {
              required: true,
            })}
          />
          {errors.username && (
            <Text color="red" mt="3px">
              {t.requiredUsername}
            </Text>
          )}
          <Label
            htmlFor="password"
            mt={10}
            mb="3px"
            sx={{
              fontSize: [0, 1, 17],
              fontWeight: 600,
            }}
          >
            {t.password}
          </Label>
          <Input
            type="password"
            defaultValue="12345678"
            placeholder={t.inputPassword}
            sx={{
              transition: "all 0.3s ease-in-out 0s",
              fontSize: [10, 0, 15],
              border: "1px solid grey",
            }}
            {...register("password", {
              required: t.requiredPassword,
              minLength: {
                value: 8,
                message: t.limit,
              },
            })}
          />
          {errors.password && (
            <Text color="red" mt="3px">
              {errors.password.message}
            </Text>
          )}

          <Button
            type="submit"
            onClick={handleSubmit(onSubmit)}
            my={25}
            sx={{
              transition: "all 0.3s ease-in-out 0s",
              opacity: 0.7,
              fontSize: [0, 1, 17],
              fontWeight: 600,
              cursor: "pointer",
              ":active": {
                transform: "scale(0.95)",
              },
              ":hover": {
                opacity: 1,
              },
            }}
          >
            {t.login}
          </Button>
          {mess && (
            <Text
              color="white"
              py={10}
              pl={10}
              mb={20}
              bg="red"
              sx={{
                fontSize: [10, 0, 15],
                borderRadius: 4,
                opacity: 0.7,
                width: "100%",
              }}
            >
              {t.wrongAccount}
            </Text>
          )}
          <Flex
            sx={{
              width: "100%",
              justifyContent: "center",
              fontSize: [10, 0, 1],
            }}
          >
            <Text mr={["3px", "5px", 10]} sx={{ opacity: 0.4 }}>
              {t.question}
            </Text>
            <Link
              href="/register"
              color="text"
              sx={{
                fontWeight: 600,
                cursor: "pointer",
                opacity: 0.6,
                transition: "all 0.3s ease-in-out 0s",
                textDecoration: "none",
                ":hover": {
                  opacity: 1,
                },
                ":active": {
                  transform: "scale(0.9)",
                },
              }}
            >
              {t.signup}
            </Link>
          </Flex>
        </Flex>
      </Flex>
    </Box>
  );
};

export default login;
