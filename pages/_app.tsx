import React, { FC } from "react";
import type { AppProps } from "next/app";
import { theme } from "../styles/theme";
import { ThemeProvider } from "theme-ui";
import { appWithTranslation } from "next-i18next";
import NProgress from "nprogress";
import "nprogress/nprogress.css";
import Router from "next/router";
import { ToastProvider } from "react-toast-notifications";
import Scroll from "../components/Scroll";
import Layout from "../containers/layout";

NProgress.configure({ showSpinner: false });
Router.events.on("routeChangeStart", () => NProgress.start());
Router.events.on("routeChangeComplete", () => {
  NProgress.done();
  window.scrollTo(0, 0);
});
Router.events.on("routeChangeError", () => NProgress.done());

const MyApp: FC<AppProps> = ({ Component, pageProps }) => (
  <ThemeProvider theme={theme}>
    <ToastProvider
      autoDismiss={true}
      autoDismissTimeout={2000}
      placement="top-center"
    >
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </ToastProvider>
    <Scroll />
  </ThemeProvider>
);

export default appWithTranslation(MyApp);
