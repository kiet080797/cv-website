import { Flex, Text, Box, Image, Button } from "theme-ui";
import React, { FC } from "react";
import Layout from "../containers/layout";
import Heading from "../components/Heading";
import { useRouter } from "next/router";
import en from "../locales/en";
import vi from "../locales/vi";
const Create: FC = () => {
  const router = useRouter();
  const t = router.locale === "en" ? en : vi;
  return (
    <Box>
      <Heading label={t.about} content={t.content} />
      <Flex
        my={30}
        sx={{
          flexDirection: ["column", "column", "row"],
          justifyContent: "center",
          alignItems: ["center", "center", "stretch"],
        }}
      >
        <Image
          mx={30}
          sx={{
            height: [200, 300, 400],
            width: [200, 300, 400],
            borderRadius: 999,
            border: ["1px solid white", "7px solid white", "15px solid white"],
            boxShadow: "rgb(0 0 0 / 10%) 0px 0px 25px 0px",
          }}
          src="images/avatar.jpg"
        />

        <Flex
          mx={30}
          sx={{
            flex: "0 0 30%",
            flexDirection: "column",
            justifyContent: "space-around",
          }}
        >
          <Flex
            sx={{
              flexDirection: "column",
              alignItems: ["center", "center", "stretch "],
            }}
          >
            <Text
              my="5px"
              sx={{
                fontSize: [14, 16, 18],
                fontWeight: 600,
                opacity: 0.4,
              }}
            >
              Front-end Javascript
            </Text>
            <Text
              my="5px"
              sx={{
                fontSize: [4, 5, 6],
                fontWeight: 600,
              }}
            >
              {t.name}
            </Text>
          </Flex>
          <Flex
            my={20}
            mx={["5px", 100, 0]}
            sx={{
              flexDirection: "column",
            }}
          >
            <Text
              mb={10}
              sx={{
                fontSize: [0, 1, 2],
                opacity: 0.8,
              }}
            >
              {t.content1}
            </Text>
            <Text
              my={10}
              sx={{
                fontSize: [0, 1, 2],
                opacity: 0.8,
              }}
            >
              {t.content2}
            </Text>
            <Text
              my={10}
              sx={{
                fontSize: [0, 1, 2],
                opacity: 0.8,
              }}
            >
              {t.content3}
            </Text>
          </Flex>
          <Flex
            mt={[0, 20, 20]}
            sx={{
              justifyContent: ["center", "center", "stretch"],
            }}
          >
            <Box mr={15}>
              <Button variant="tertiary">Download CV</Button>
            </Box>
            <Box>
              <Button variant="secondary">View Transcript</Button>
            </Box>
          </Flex>
        </Flex>
      </Flex>
    </Box>
  );
};

export default Create;
