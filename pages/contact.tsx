import { Flex, Box, Grid } from "theme-ui";
import React, { FC } from "react";
import Heading from "../components/Heading";
import ContactItem from "../components/ContactItem";
import PhoneIcon from "../public/Icon/phone.svg";
import MailIcon from "../public/Icon/mail.svg";
import AddressIcon from "../public/Icon/home.svg";
import FaceIcon from "../public/Icon/facebook1.svg";
import InsTagIcon from "../public/Icon/instagram1.svg";
import WebIcon from "../public/Icon/world.svg";
import { useRouter } from "next/router";
import en from "../locales/en";
import vi from "../locales/vi";
const Contact: FC = () => {
  const router = useRouter();
  const t = router.locale === "en" ? en : vi;
  const ContactItems = [
    {
      label: router.locale === "en" ? "Phone Number" : "Số điện thoại",
      address: "0347551122",
      icon: <PhoneIcon />,
    },
    {
      label: "Email",
      address: "kiet080797@gmail.com",
      icon: <MailIcon />,
    },
    {
      label: router.locale === "en" ? "Address" : "Địa chỉ",
      address:
        router.locale === "en"
          ? "Ward 13, Binh Thanh District, HCM city"
          : "Phường 13, Quận Bình Thạnh, Tp. HCM",
      icon: <AddressIcon />,
    },
    {
      label: "Facebook",
      address: "https://www.facebook.com/kietnguyen.0807",
      icon: <FaceIcon />,
    },
    {
      label: "Instagram",
      address: "instagram.com/kiet.87",
      icon: <InsTagIcon />,
    },
    {
      label: "Website",
      address: "kietnguyendev.com",
      icon: <WebIcon />,
    },
  ];

  return (
    <Box>
      <Heading label={t.contact} content={t.content8} />
      <Flex
        sx={{
          width: "100%",
          justifyContent: "center",
        }}
      >
        <Flex
          mt={[20, 30, 40]}
          sx={{
            flexDirection: "column",
            width: ["90%", "70%", "55%"],
          }}
        >
          <Box
            sx={{
              iframe: {
                width: "100%",
                height: [150, 200, 250],
                borderRadius: 5,
                border: "none",
              },
            }}
          >
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15675.133223850962!2d106.7041219!3d10.8278884!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3175288b8b751ec1%3A0x54ad16763769cd2!2zcGjGsOG7nW5nIDEzLCBCw6xuaCBUaOG6oW5oLCBUaMOgbmggcGjhu5EgSOG7kyBDaMOtIE1pbmg!5e0!3m2!1svi!2s!4v1621344905209!5m2!1svi!2s"
              loading="lazy"
            />
          </Box>
          <Grid columns={[1, 1, 2]} mt={[20, 30, 30]} gap={0}>
            {ContactItems.map((item, index) => (
              <Box mt={[10, 15, 30]} key={index}>
                <ContactItem
                  label={item.label}
                  address={item.address}
                  icon={item.icon}
                />
              </Box>
            ))}
          </Grid>
        </Flex>
      </Flex>
    </Box>
  );
};

export default Contact;
