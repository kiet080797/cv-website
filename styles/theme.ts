import { Theme } from "theme-ui";

export const theme: Theme = {
  breakpoints: ["600px", "825px"],
  fontSizes: [12, 14, 16, 20, 24, 32, 48, 64],

  colors: {
    text: "#000",
    background: "#fff",
    backgroundHeading: "rgb(240, 240, 240)",
    bgToggleMenu: "rgb(200, 200, 200)",
    switch: "rgb(200, 200, 200)",
    primary: "#1EA7FD",
    secondary: "grey",
    white: "#fff",
    bigStone: "#1C2D41",
    outerSpace: "#202E2E",
    chelseaCucumber: "#7DA751",
    chelseaCucumber18: "rgba(125, 167, 81, 0.18)",
    mistGray: "#C2C2B5",
    border: "rgb(235, 235, 235)",

    modes: {
      dark: {
        bgToggleMenu: "rgb(70, 68, 68)",
        backgroundHeading: "rgb(49, 48, 48)",
        text: "#fff",
        background: "#000",
        primary: "#0cf",
        border: "rgb(70, 68, 68)",
      },
    },
  },

  forms: {
    switch: {
      bg: "switch",
      position: "absolute",
      mt: ["-7px", -9, -11],
      width: [22, 28, 41],
      height: [13, 16, 23],
      cursor: "pointer",
      boxShadow: "rgb(0 0 0 / 10%) 1px 3px 3px 1px",
      transition: "all 0.3s ease-in-out 0s",
      opacity: 0.6,
      ":hover": {
        opacity: 1,
      },
      ":active": {
        transform: "scale(0.9)",
        boxShadow: "rgb(0 0 0 / 10%) 1px 3px 3px 1px",
      },
      div: {
        height: [9, 12, 18],
        width: [9, 12, 18],
      },
    },
  },

  buttons: {
    secondary: {
      py: ["7px", 10, 10],
      px: [10, 15, 20],
      fontSize: [10, 0, 1],
      lineHeight: 1.2,
      borderRadius: 40,
      border: "2px solid grey",
      fontWeight: 600,
      color: "rgb(152, 152, 152)",
      bg: "white",
      boxShadow: "rgb(0 0 0 / 22%) 0px 10px 10px -8px",
      transition: "all 0.3s ease-in-out 0s",
      cursor: "pointer",
      ":hover": {
        bg: "grey",
        color: "white",
      },
      ":active": {
        transform: "scale(0.95)",
      },
    },
    tertiary: {
      py: ["7px", 10, 10],
      px: [10, 15, 20],
      fontSize: [10, 0, 1],
      lineHeight: 1.2,
      borderRadius: 40,
      border: "2px solid #1EA7FD",
      fontWeight: 600,
      color: "rgb(102, 102, 102)",
      bg: "white",
      boxShadow: "rgb(0 0 0 / 22%) 0px 10px 10px -8px",
      transition: "all 0.3s ease-in-out 0s",
      cursor: "pointer",
      ":hover": {
        bg: "primary",
        color: "white",
      },
      ":active": {
        transform: "scale(0.95)",
      },
    },
  },
  links: {
    bold: {
      fontWeight: "bold",
    },
    nav: {
      color: "white",
      fontWeight: "bold",
      textDecoration: "none",
    },
  },
  fontWeights: {
    body: 500,
    heading: 900,
  },
};
