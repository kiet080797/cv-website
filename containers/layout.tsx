import React, { FC, PropsWithChildren } from "react";
import { Box } from "theme-ui";
import NavigationBar from "../components/Header";
import Footer from "../components/Footer";
import { useRouter } from "next/router";
import en from "../locales/en";
import vi from "../locales/vi";

const Layout: FC<PropsWithChildren<Record<string, unknown>>> = ({
  children,
}) => {
  const router = useRouter();
  const t = router.locale === "en" ? en : vi;
  return (
    <Box
      sx={{
        width: "100%",
        minHeight: "100vh",
        justifyContent: "center",
      }}
    >
      <NavigationBar
        about={t.about}
        resume={t.resume}
        contact={t.contact}
        language={t.language}
        modeDark={t.darkMode}
        login={t.login}
        signOut={t.signOut}
      />
      {children}
      <Footer />
    </Box>
  );
};

export default Layout;
