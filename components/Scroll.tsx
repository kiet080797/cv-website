import React, { useState, useEffect } from "react";
import { Flex } from "theme-ui";
import ArrowIcon from "../public/Icon/arrow.svg";

const Scroll = () => {
  const [showScroll, setShowScroll] = useState(false);
  const logit = () => {
    window.pageYOffset >= 150 ? setShowScroll(true) : setShowScroll(false);
  };
  useEffect(() => {
    const watchScroll = () => {
      window.addEventListener("scroll", logit);
    };

    watchScroll();
    return () => {
      window.removeEventListener("scroll", logit);
    };
  });
  return (
    <Flex
      onClick={() => {
        window.scrollTo({ top: 0, behavior: "smooth" });
      }}
      bg="background"
      sx={{
        visibility: showScroll ? "visible" : "hidden",
        height: [30, 40, 50],
        width: [30, 40, 50],
        position: "fixed",
        justifyContent: "center",
        alignItems: "center",
        bottom: 10,
        right: 15,
        zIndex: 100,
        transition: "opacity 0.33s ease-in-out 0s",
        borderRadius: 8,
        border: [
          "1px solid rgb(200, 200, 200)",
          "2px solid rgb(200, 200, 200)",
        ],
        cursor: "pointer",
        opacity: 0.6,

        svg: {
          transform: "rotate(180deg)",
          height: [10, 15, 20],
          width: [10, 15, 20],
        },
        ":hover": {
          color: "primary",
          opacity: 1,
          boxShadow: "rgb(0 0 0 / 10%) 0px 0px 12px 0px",
        },
        ":active": {
          transform: "scale(0.95)",
        },
      }}
    >
      <ArrowIcon />
    </Flex>
  );
};

export default Scroll;
