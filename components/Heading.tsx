import { Flex, Text } from "theme-ui";
import React, { FC } from "react";

export interface HeadingProps {
  label: string;
  content: string;
}

const Heading: FC<HeadingProps> = ({ label, content }) => {
  return (
    <Flex
      mt={[35, 50, 0]}
      bg="backgroundHeading"
      sx={{
        opacity: 0.8,
        flexDirection: "column",
        height: [60, 80, 100],
        width: "100%",
      }}
    >
      <Flex
        sx={{
          height: "100%",
          flexDirection: "column",
          justifyContent: "center",
          textAlign: "center",
        }}
      >
        <Text
          sx={{
            fontSize: [20, 25, 30],
            fontWeight: 600,
          }}
        >
          {label}
        </Text>
        <Text
          sx={{
            fontStyle: "italic",
            fontSize: [9, 10, 0],

            opacity: 0.6,
          }}
        >
          {content}
        </Text>
      </Flex>
    </Flex>
  );
};

export default Heading;
