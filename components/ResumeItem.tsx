import { Flex, Text } from "theme-ui";
import React, { FC } from "react";

interface ResumeItemProps {
  date: string;
  place: string;
  label: string;
  content: string;
}

const ResumeItem: FC<ResumeItemProps> = ({ date, place, label, content }) => {
  return (
    <Flex
      ml={25}
      mb={[20, 35, 50]}
      pl={10}
      sx={{
        borderLeft: "2px solid rgb(30,167,253, 0.2)",
        flexDirection: "column",
        lineHeight: 1.6,
      }}
    >
      <Text
        sx={{
          fontSize: [0, 1, 2],
          alignItem: "center",
          fontWeight: 600,
        }}
      >
        <Text
          bg="background"
          ml={-40}
          px={10}
          py="5px"
          mr="5px"
          sx={{
            border: "2px solid #1EA7FD",
            borderRadius: 40,
          }}
        >
          {date}
        </Text>
        <Text
          sx={{
            opacity: 0.5,
            lineHeight: 2.3,
          }}
        >
          {place}
        </Text>
      </Text>
      <Flex
        mt="5px"
        sx={{
          flexDirection: "column",
        }}
      >
        <Text
          sx={{
            fontWeight: "600",
            fontSize: [0, 1, 2],
          }}
        >
          {label}
        </Text>
        <Text
          mb="1em"
          mt="0.5em"
          sx={{
            fontSize: [0, 1, 2],
            lineHeight: "1.75em",
            opacity: 0.8,
          }}
        >
          {content}
        </Text>
      </Flex>
    </Flex>
  );
};

export default ResumeItem;
