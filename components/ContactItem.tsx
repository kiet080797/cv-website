import React, { FC, ReactNode } from "react";
import { Flex, Box, Text } from "theme-ui";

export interface ContactItemProps {
  label: string;
  address: string;
  icon: ReactNode;
}

const ContactItem: FC<ContactItemProps> = ({ label, address, icon }) => {
  return (
    <Flex>
      <Box
        mx={[10, 20, 30]}
        sx={{
          justifyContent: "center",
          alignItems: "center",
          svg: {
            width: [30, 35, 40],
            height: [30, 35, 40],
            fill: "#1EA7FD",
          },
        }}
      >
        {icon}
      </Box>
      <Flex
        sx={{
          flexDirection: "column",
        }}
      >
        <Text
          sx={{
            fontSize: [14, 16, 18],
            fontWeight: 600,
            opacity: 0.8,
          }}
        >
          {label}
        </Text>
        <Text
          m={["5px", "7px", "7px"]}
          sx={{
            fontSize: [0, 1, 2],
            fontWeight: 400,
            lineHeight: "1.75em",
            opacity: 0.8,
          }}
        >
          {address}
        </Text>
      </Flex>
    </Flex>
  );
};

export default ContactItem;
