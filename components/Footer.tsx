import { Flex, Text, Box, Link, Grid } from "theme-ui";
import React from "react";
import FbIcon from "../public/Icon/facebook.svg";
import InsIcon from "../public/Icon/instagram.svg";
import TwitIcon from "../public/Icon/twitter.svg";
import YtbIcon from "../public/Icon/youtube.svg";

const Footer = () => {
  return (
    <Flex
      sx={{
        width: "100%",
        justifyContent: "center",
      }}
    >
      <Flex
        mt={[10, 30, 50]}
        p={[10, 15, 15]}
        sx={{
          width: ["90%", "70%", "55%"],
          flexWrap: "wrap",
          alignItem: "center",
          borderTop: "1px solid rgba(179, 178, 178, 0.2)",
        }}
      >
        {" "}
        <Grid
          columns={[1, 1, 2]}
          gap={0}
          sx={{
            width: "100%",
            textAlign: ["center", "center", "left"],
          }}
        >
          <Text
            mb={[10, 15, 15]}
            sx={{
              justifyContent: "center",
              fontSize: [0, 1, 2],
              opacity: 0.7,
            }}
          >
            Copyright © 2021 All Rights Reserved by KietDev.
          </Text>
          <Flex
            sx={{
              maxWidth: "100%",
              justifyContent: ["center", "center", "flex-end"],
            }}
          >
            <Box
              mx={15}
              sx={{
                opacity: 0.6,
                ":hover": {
                  opacity: 1,
                },
                transition: "all 0.12s ease-in-out 0s",
                cursor: "pointer",
                ":active": {
                  transform: "scale(0.95)",
                },
                svg: {
                  width: [13, 15, 19],
                  height: [13, 15, 19],
                },
              }}
            >
              <Link color="text" href="#!">
                <FbIcon />
              </Link>
            </Box>
            <Box
              mx={15}
              sx={{
                opacity: 0.6,
                ":hover": {
                  opacity: 1,
                },
                transition: "all 0.12s ease-in-out 0s",
                cursor: "pointer",

                ":active": {
                  transform: "scale(0.95)",
                },
                svg: {
                  width: [13, 15, 19],
                  height: [13, 15, 19],
                },
              }}
            >
              <Link color="text" href="#!">
                <InsIcon />
              </Link>
            </Box>
            <Box
              mx={15}
              sx={{
                opacity: 0.6,
                ":hover": {
                  opacity: 1,
                },
                transition: "all 0.12s ease-in-out 0s",
                cursor: "pointer",
                ":active": {
                  transform: "scale(0.95)",
                },
                svg: {
                  width: [13, 15, 19],
                  height: [13, 15, 19],
                },
              }}
            >
              <Link color="text" href="#!">
                <TwitIcon />
              </Link>
            </Box>
            <Box
              mx={15}
              sx={{
                opacity: 0.6,
                ":hover": {
                  opacity: 1,
                },
                transition: "all 0.12s ease-in-out 0s",
                cursor: "pointer",
                ":active": {
                  transform: "scale(0.95)",
                },
                svg: {
                  width: [13, 15, 19],
                  height: [13, 15, 19],
                },
              }}
            >
              <Link color="text" href="#!">
                <YtbIcon />
              </Link>
            </Box>
          </Flex>
        </Grid>
      </Flex>
    </Flex>
  );
};

export default Footer;
