import React, { FC } from "react";
import { Flex, Text } from "theme-ui";
import { useRouter } from "next/router";
import Link from "next/link";
import KIcon from "../public/Icon/k.svg";
import Menu from "./Menu";
import { MenuProps } from "./Menu";

const NavigationBar: FC<MenuProps> = ({
  about,
  resume,
  contact,
  language,
  modeDark,
  login,
  signOut,
}) => {
  const router = useRouter();
  return (
    <Flex
      bg="background"
      sx={{
        position: ["fixed", "fixed", "relative"],
        top: 0,
        left: 0,
        right: 0,
        width: "100%",
        height: [35, 50, 80],
        justifyContent: "space-between",
        alignItems: "center",
        zIndex: 99,
        borderBottom: "1px solid rgba(179, 178, 178, 0.2)",
        boxShadow: [
          "rgb(0 0 0 / 10%) 1px 3px 3px 1px",
          "rgb(0 0 0 / 10%) 1px 3px 3px 1px",
          "none",
        ],
      }}
    >
      <Link href="/" locale={router.locale}>
        <Flex
          px={15}
          sx={{
            cursor: "pointer",
            color: "text",
            textDecoration: "none",
            alignItems: "center",
            svg: {
              height: [22, 30, 40],
              width: [22, 30, 40],
            },
          }}
        >
          <KIcon />
          <Text
            px={["6px", "8px", 10]}
            sx={{
              opacity: 0.7,
              ":hover": {
                opacity: 1,
              },
              fontSize: ["8px", 1, 3],
            }}
          >
            <Text
              sx={{
                fontWeight: 800,
              }}
            >
              Kiet's
            </Text>{" "}
            CV
          </Text>
        </Flex>
      </Link>
      <Menu
        about={about}
        resume={resume}
        contact={contact}
        language={language}
        modeDark={modeDark}
        login={login}
        signOut={signOut}
      />
    </Flex>
  );
};

export default NavigationBar;
