import React, { FC, useState, useRef, useEffect, useCallback } from "react";
import useClickOutside from "use-click-outside";
import LanguageButton from "./LanguageButton";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import Link from "next/link";
import { Flex, Text, Box, useColorMode, Switch } from "theme-ui";
import { useRouter } from "next/router";
import MenuIcon from "../public/Icon/menu.svg";
import CloseIcon from "../public/Icon/close.svg";
import LoginIcon from "../public/Icon/enter.svg";
import UserIcon from "../public/Icon/user.svg";

export interface MenuProps {
  about: string;
  resume: string;
  contact: string;
  language: string;
  modeDark: string;
  login: string;
  signOut: string;
}

export const ChangeModeButton = () => {
  const [colorMode, setColorMode] = useColorMode();

  return (
    <Box>
      <Switch
        checked={colorMode === "dark"}
        onChange={() => {
          setColorMode(colorMode === "default" ? "dark" : "default");
        }}
      />
    </Box>
  );
};

const MenuToggle: FC<MenuProps> = ({
  about,
  resume,
  contact,
  language,
  modeDark,
  login,
  signOut,
}) => {
  const isMobile = useMediaQuery("(max-width: 825px)");
  const router = useRouter();
  const ref = useRef<HTMLDivElement>(null);
  const [isOpen, setIsOpen] = useState(false);
  useClickOutside(ref, () => setIsOpen(false));
  const [username, setUsername] = useState("");

  useEffect(() => {
    const name = localStorage.getItem("username");
    setUsername(name ? name : "");
  }, [username]);

  const handleClick = () => {
    setIsOpen(!isOpen);
  };

  const logout = useCallback(() => {
    localStorage.removeItem("token");
    localStorage.removeItem("username");
    router.push("/login");
    setUsername("");
  }, []);

  const signIn = useCallback(() => {
    router.push("/login");
  }, []);

  return (
    <Flex ref={ref}>
      <Box
        onClick={handleClick}
        p={["8px", 15, 15]}
        mr={["8px", 10, 10]}
        mt={["4px", 0, 0]}
        sx={{
          opacity: [0.6, 0.6, 0],
          cursor: ["pointer", "pointer", "auto"],
          ":hover": {
            opacity: [1, 1, 0],
          },
        }}
      >
        <Box
          sx={{
            position: "absolute",
            opacity: isOpen ? 0 : 1,
            svg: {
              fill: "text",
              height: [15, 20, 20],
              width: [15, 20, 20],
            },
          }}
        >
          <MenuIcon />
        </Box>
        <Box
          sx={{
            opacity: isOpen ? 1 : 0,
            svg: {
              fill: "text",
              height: [12, 17, 17],
              width: [12, 17, 17],
            },
          }}
        >
          <CloseIcon />
        </Box>
      </Box>
      <Flex
        bg="background"
        sx={{
          alignItems: ["left", "left", "center"],
          width: [130, 200, "auto"],
          height: ["100%", "100%", 70],
          top: [35, 50, "unset"],
          right: [isOpen ? 0 : -200, isOpen ? 0 : -200, "unset"],
          position: ["fixed", "fixed", "relative"],
          flexDirection: ["column", "column", "row"],
          transition: [
            "0.3s ease-in-out 0s right, opacity",
            "0.3s ease-in-out 0s right, opacity",
            "none",
          ],
          borderLeft: [
            "1px solid rgba(179, 178, 178, 0.2)",
            "1px solid rgba(179, 178, 178, 0.2)",
            "1px solid transparent",
          ],
          zIndex: 999,
        }}
      >
        <Link href="/about" locale={router.locale}>
          <Text
            mt={[15, 15, 0]}
            py={[0, 13, 13]}
            px={[15, 20, 20]}
            sx={{
              cursor: "pointer",
              color: "text",
              textDecoration: "none",
              fontSize: [9, 0, 2],
              fontWeight: 600,
              opacity: router.pathname === "/about" ? 1 : 0.6,
              ":hover": {
                opacity: 1,
              },
            }}
          >
            {about}
          </Text>
        </Link>
        <Link href="/resume" locale={router.locale}>
          <Text
            mt={[15, 0, 0]}
            py={[0, 13, 13]}
            px={[15, 20, 20]}
            sx={{
              cursor: "pointer",
              fontSize: [9, 0, 2],
              fontWeight: 600,
              opacity: router.pathname === "/resume" ? 1 : 0.6,
              color: "text",
              textDecoration: "none",
              ":hover": {
                opacity: 1,
              },
            }}
          >
            {resume}
          </Text>
        </Link>
        <Link href="/contact" locale={router.locale}>
          <Text
            mt={[15, 0, 0]}
            py={[0, 13, 13]}
            px={[15, 20, 20]}
            sx={{
              cursor: "pointer",
              fontSize: [9, 0, 2],
              fontWeight: 600,
              opacity: router.pathname === "/contact" ? 1 : 0.6,
              color: "text",
              textDecoration: "none",
              ":hover": {
                opacity: 1,
              },
            }}
          >
            {contact}
          </Text>
        </Link>
        <Flex
          mt={[12, 0, 64]}
          py={[0, "7px", 9]}
          px={[15, 20, 20]}
          sx={{
            fontSize: [9, 0, 2],
          }}
        >
          <LanguageButton languageLabel={language} />
        </Flex>

        <Flex
          mr={30}
          mt={[13, 0, 0]}
          py={[0, 12, 10]}
          pl={[15, 20, 15]}
          pr={[0, 20, 15]}
          sx={{
            position: "relative",
            alignItems: "center",
            opacity: 0.6,
            ":hover": {
              opacity: 1,
            },
          }}
        >
          <Text
            mr={["7px", 10, 10]}
            sx={{
              fontSize: [9, 0, 2],
              fontWeight: 600,
              color: "text",
            }}
          >
            {modeDark}
          </Text>
          <ChangeModeButton />
        </Flex>

        <Flex
          mt={[17, 0, 0]}
          py={[0, 13, 13]}
          px={[15, 20, 20]}
          sx={{
            cursor: "pointer",
            fontSize: [9, 0, 2],
            fontWeight: 600,
            color: "text",
            textDecoration: "none",
            svg: {
              fill: "text",
              width: 17,
              height: 17,
              opacity: 0.6,
            },
          }}
        >
          {username === "" ? (
            <Flex
              sx={{
                opacity: router.pathname === "/login" ? 1 : 0.6,
                ":hover": {
                  opacity: 1,
                },
              }}
            >
              {!isMobile && <LoginIcon />}
              <Text onClick={signIn} ml={[0, 0, 10]} mr={30}>
                {login}
              </Text>
            </Flex>
          ) : (
            <Flex>
              {!isMobile && (
                <Flex
                  pr={[0, 0, "5px"]}
                  sx={{
                    borderRight: "2px solid",
                    opacity: 0.6,
                    ":hover": {
                      opacity: 1,
                    },
                  }}
                >
                  <UserIcon />
                  <Text
                    ml={[0, 0, 10]}
                    sx={{
                      overflow: "hidden",
                    }}
                  >
                    {username}
                  </Text>
                </Flex>
              )}
              <Text
                ml={[0, 0, "5px"]}
                sx={{
                  opacity: 0.6,
                  ":hover": {
                    opacity: 1,
                  },
                }}
                onClick={logout}
              >
                {signOut}
              </Text>
            </Flex>
          )}
        </Flex>
      </Flex>
    </Flex>
  );
};

export default MenuToggle;
