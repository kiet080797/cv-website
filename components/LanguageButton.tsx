import { Flex, Text, Box } from "theme-ui";
import React, { FC, useState, useRef } from "react";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import VietnamIcon from "../public/Icon/vietnam.svg";
import EnglandIcon from "../public/Icon/england.svg";
import { useRouter } from "next/router";
import useClickOutside from "use-click-outside";

interface LanguageProps {
  languageLabel: string;
}

const LanguageButton: FC<LanguageProps> = ({ languageLabel }) => {
  const isMobile = useMediaQuery("(max-width: 825px)");
  const [isOpen, setIsOpen] = useState(false);
  const router = useRouter();
  const ref = useRef<HTMLDivElement>(null);
  useClickOutside(ref, () => setIsOpen(false));

  return (
    <Flex
      ref={ref}
      sx={{
        flexDirection: "column",
      }}
    >
      <Flex
        sx={{
          alignItems: "center",
          fontWeight: 600,
          opacity: isOpen ? 1 : 0.6,
          ":hover": {
            opacity: 1,
          },
        }}
      >
        <Text color="text" sx={{ overflow: "hidden" }}>
          {languageLabel}
        </Text>
        {!isMobile && (
          <Box
            onClick={() => setIsOpen(!isOpen)}
            ml="5px"
            sx={{
              cursor: "pointer",
              transition: "all 0.3s ease-in-out 0s",
              svg: {
                height: 30,
                width: "auto",
              },
              ":active": {
                transform: "scale(0.9)",
              },
            }}
          >
            {router.locale === "vi" && <VietnamIcon />}

            {router.locale === "en" && <EnglandIcon />}
          </Box>
        )}

        {isMobile && (
          <Box
            onClick={() =>
              router.push(router.pathname, router.pathname, {
                locale: router.locale === "en" ? "vi" : "en",
              })
            }
            ml="10px"
            sx={{
              cursor: "pointer",
              transition: "all 0.3s ease-in-out 0s",
              svg: {
                height: [17, 25, 30],
                width: "auto",
              },
              ":active": {
                transform: "scale(0.9)",
              },
            }}
          >
            {router.locale === "vi" && <VietnamIcon />}

            {router.locale === "en" && <EnglandIcon />}
          </Box>
        )}
      </Flex>

      {!isMobile && (
        <Flex
          bg="bgToggleMenu"
          sx={{
            right: -78,
            position: "relative",
            justifyContent: "center",
            borderRadius: 5,
            flexDirection: "column",
            fontWeight: 600,
            opacity: isOpen ? 1 : 0,
            boxShadow: "rgb(0 0 0 / 10%) 1px 3px 3px 1px",
            transition: "all 0.3s ease-in-out 0s",
            zIndex: 99,
          }}
        >
          <Text
            onClick={() => {
              setIsOpen(false);
              isOpen &&
                router.push(router.pathname, router.pathname, { locale: "vi" });
            }}
            px={10}
            py="7px"
            sx={{
              cursor: isOpen && "pointer",
              width: "100%",
              ":hover": {
                borderRadius: 5,
                bg: "#1EA7FD",
                color: "white",
              },
            }}
          >
            Vietnamese
          </Text>

          <Text
            onClick={() => {
              setIsOpen(false);
              isOpen &&
                router.push(router.pathname, router.pathname, { locale: "en" });
            }}
            px={10}
            py="7px"
            sx={{
              cursor: isOpen && "pointer",
              width: "100%",
              ":hover": {
                borderRadius: 5,
                bg: "#1EA7FD",
                color: "white",
              },
            }}
          >
            England
          </Text>
        </Flex>
      )}
    </Flex>
  );
};

export default LanguageButton;
